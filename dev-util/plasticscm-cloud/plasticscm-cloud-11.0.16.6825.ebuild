# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="https://www.plasticscm.com/plasticrepo/stable/debian/amd64/plasticscm-cloud_11.0.16.6825_amd64.deb"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="dev-util/plasticscm-server-core
dev-util/plasticscm-client-complete"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack "$A"
	cd "$WORKDIR"
	mkdir -p "$P"
	[ -f 'data.tar.xz' ] || die "Archive not found"
	tar xaf 'data.tar.xz' -C "$WORKDIR/$P"
	rm 'data.tar.xz'
	cd -
}

src_install() {
	cp -a "$S/opt" "$D"
	cp -a "$S/usr" "$D"
}

pkg_preinst() {
	cloud_token_file="/opt/plasticscm5/client/cloudedition.token"
	dvcs_token_file="/opt/plasticscm5/client/dvcs.token"
	current_license="/opt/plasticscm5/server/plasticd.lic"
	backup_license_file="/opt/plasticscm5/cloud/plasticd.lic.bkp"

	if [ ! -f "${cloud_token_file}" ] && [ ! -f "${dvcs_token_file}" ]; then
		if [ -f "${current_license}" ]; then
			if [ ! -d "/opt/plasticscm5/cloud" ]; then
				mkdir "/opt/plasticscm5/cloud"
			fi
			#TODO why... just why
			cp "${current_license}" "${backup_license_file}" || true
		fi
	fi
}

pkg_postinst() {
	cloud_token_file="/opt/plasticscm5/client/cloudedition.token"
	current_license="/opt/plasticscm5/server/plasticd.lic"
	new_license="/opt/plasticscm5/cloud/plasticd.lic"

	if [ ! -f "${cloud_token_file}" ]; then
		touch "${cloud_token_file}" || true
	fi

	cp "${new_license}" "${current_license}" || true
	#if [ -f "/etc/init.d/plasticsd" ]; then
	#    /etc/init.d/plasticsd restart || true
	#fi
	#systemctl try-restart plasticscm-server >/dev/null 2>&1 || true
	ewarn "Please restart plasticscm-server after upgrading"
}

pkg_postrm() {
	cloud_token_file="/opt/plasticscm5/client/cloudedition.token"
	backup_license_file="/opt/plasticscm5/cloud/plasticd.lic.bkp"
	server_license_file="/opt/plasticscm5/server/plasticd.lic"
	server_license_token_file="/opt/plasticscm5/server/plasticd.token.lic"

	if [ -f "${cloud_token_file}" ]; then
		rm "${cloud_token_file}" || true
	fi

	if [ -f "${server_license_file}" ]; then
		rm "${server_license_file}" || true
	fi

	if [ -f "/opt/plasticscm5/server/plasticd" ]; then
		if [ -f "${backup_license_file}" ]; then
			cp "${backup_license_file}" "${server_license_file}" || true
		fi

		#TODO: WTF. If no licence... use enterprise licence? In the removal phase? Absolutely nonsense.
		#if [ ! -f "${server_license_file}" ] && [ ! -f "${server_license_token_file}" ]; then
		#/opt/plasticscm5/server/plasticd configure --language=en --workingmode=NameWorkingMode --port=8087 --license=Enterprise || true
		#fi

		#if [ -f "/etc/init.d/plasticsd" ]; then
		#    /etc/init.d/plasticsd restart || true
		#fi
		#systemctl try-restart plasticscm-server >/dev/null 2>&1 || true
	fi
	ewarn "You've uninstalled plastic: stop the server maybe?"
}
