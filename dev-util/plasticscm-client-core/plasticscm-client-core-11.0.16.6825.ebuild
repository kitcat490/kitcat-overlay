# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="https://www.plasticscm.com/plasticrepo/stable/debian/amd64/plasticscm-client-core_11.0.16.6825_amd64.deb"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="dev-util/plasticscm-mono4
dev-util/plasticscm-certtools-mono4"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack "$A"
	cd "$WORKDIR"
	mkdir -p "$P"
	[ -f 'data.tar.xz' ] || die "Archive not found"
	tar xaf 'data.tar.xz' -C "$WORKDIR/$P"
	rm 'data.tar.xz'
	cd -
}

src_install() {
	cp -a "$S/opt" "$D"
	cp -a "$S/usr" "$D"
}

pkg_preinst() {
	client_dir="/opt/plasticscm5/client"
	old_remoting_client_file="/opt/plasticscm5/client/remoting.conf"

	if [ -f "${old_remoting_client_file}" ]; then
		rm -f "${old_remoting_client_file}" || die "error removing remoting.conf"
	fi

	if [ -d "${client_dir}" ]; then
		find "${client_dir}" -name "*.conf" -exec cp {} {}.bkp \;
	fi
}

pkg_postinst() {
	client_dir="/opt/plasticscm5/client"

	if [ -e "/opt/plasticscm5/mono/etc/mono/4.5" ]; then
		ln -s /opt/plasticscm5/mono/etc/mono/4.0 /opt/plasticscm5/mono/etc/mono/4.5
	fi

	if [ -d "$client_dir" ]; then
		for conf_file_bkp in `find "$client_dir" -type f -name "*.conf.bkp`; do
			conf_file=$(echo "$conf_file_bkp" | sed 's/^\(.*\)\.bkp$/\1/')
			mv "$conf_file_bkp" "$conf_file"
		done
	fi
}
