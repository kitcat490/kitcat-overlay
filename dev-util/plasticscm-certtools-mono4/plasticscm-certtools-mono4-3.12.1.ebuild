# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="https://www.plasticscm.com/plasticrepo/stable/debian/amd64/plasticscm-certtools-mono4_3.12.1_amd64.deb"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="dev-util/plasticscm-mono4
app-misc/ca-certificates"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack "$A"
	cd "$WORKDIR"
	mkdir -p "$P"
	[ -f 'data.tar.xz' ] || die "Archive not found"
	tar xaf 'data.tar.xz' -C "$WORKDIR/$P"
	rm 'data.tar.xz'
	cd -
}

src_install() {
	cp -a "$S/opt" "$D"
	cp -a "$S/usr" "$D"
}


pkg_postinst() {
	#TODO
	if [ -x /opt/plasticscm5/mono/bin/cert-sync ]; then
		#TODO
		update-ca-certificates || die "Failed to update certificates"
		[ -f /etc/ssl/certs/ca-certificates.crt ] && /opt/plasticscm5/mono/bin/cert-sync /etc/ssl/certs/ca-certificates.crt || die "Certificate sync failed"
	fi
	/opt/plasticscm5/certtools/mozroots --import --machine --add-only || die "Cert config failed"
	/opt/plasticscm5/certtools/certmgr -ssl -m -y https://www.plasticscm.com/ || die "Cert config failed"
	/opt/plasticscm5/certtools/certmgr -ssl -m -y https://cloud.plasticscm.com/ || die "Cert config failed"
}
