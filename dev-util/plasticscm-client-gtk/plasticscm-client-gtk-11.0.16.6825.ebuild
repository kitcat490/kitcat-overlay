# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit xdg-utils

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="https://www.plasticscm.com/plasticrepo/stable/debian/amd64/plasticscm-client-gtk_11.0.16.6825_amd64.deb"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="dev-util/plasticscm-theme
dev-util/plasticscm-client-core
dev-util/plasticscm-gtk-sharp-mono4
dev-util/plasticscm-gnome-sharp-mono4"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack "$A"
	cd "$WORKDIR"
	mkdir -p "$P"
	[ -f 'data.tar.xz' ] || die "Archive not found"
	tar xaf 'data.tar.xz' -C "$WORKDIR/$P"
	rm 'data.tar.xz'
	cd -
}

src_install() {
	cp -a "$S/opt" "$D"
	cp -a "$S/usr" "$D"
}

pkg_postinst() {
	if [ ! -e "/opt/plasticscm5/mono/etc/mono/4.5" ]; then
		ln -s /opt/plasticscm5/mono/etc/mono/4.0 /opt/plasticscm5/mono/etc/mono/4.5
	fi

	xdg-mime default /usr/share/applications/plasticx.desktop x-scheme-handler/plastic

	xdg_icon_cache_update
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_icon_cache_update
	xdg_desktop_database_update
	xdg_mimeinfo_database_update
}
