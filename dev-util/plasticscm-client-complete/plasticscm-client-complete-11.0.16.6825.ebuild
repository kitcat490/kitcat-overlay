# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="https://www.plasticscm.com/plasticrepo/stable/debian/amd64/plasticscm-client-complete_11.0.16.6825_amd64.deb"

LICENSE="plastic"
SLOT="0"
KEYWORDS="~amd64"

DEPEND="dev-util/plasticscm-client-core
dev-util/plasticscm-client-gtk
dev-util/plasticscm-theme"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack "$A"
	cd "$WORKDIR"
	mkdir -p "$P"
	[ -f 'data.tar.xz' ] || die "Archive not found"
	tar xaf 'data.tar.xz' -C "$WORKDIR/$P"
	rm 'data.tar.xz'
	cd -
}

src_install() {
	cp -a "$S/usr" "$D"
}
