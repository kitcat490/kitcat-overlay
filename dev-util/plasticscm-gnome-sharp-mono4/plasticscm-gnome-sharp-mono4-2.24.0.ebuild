# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION=""
HOMEPAGE=""
SRC_URI="https://www.plasticscm.com/plasticrepo/stable/debian/amd64/plasticscm-gnome-sharp-mono4_2.24.0_amd64.deb"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="dev-util/plasticscm-gtk-sharp-mono4
gnome-base/libgnomecanvas"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack "$A"
	cd "$WORKDIR"
	mkdir -p "$P"
	[ -f 'data.tar.xz' ] || die "Archive not found"
	tar xaf 'data.tar.xz' -C "$WORKDIR/$P"
	rm 'data.tar.xz'
	cd -
}

src_install() {
	cp -a "$S/opt" "$D"
	cp -a "$S/usr" "$D"
}

pkg_postinst() {
	env-update
}

pkg_postrm() {
	env-update
}
