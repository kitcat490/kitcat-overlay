# Copyright 2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit udev eutils xdg-utils

DESCRIPTION="Flashprint 3d printer software"
HOMEPAGE=""
SRC_URI="https://en.fss.flashforge.com/10000/software/381b6e81ee43825019157b657839885b.deb"

LICENSE=""
SLOT="0"
KEYWORDS="~amd64"

DEPEND="dev-embedded/libftdi:1
dev-libs/hidapi
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_unpack() {
	unpack "$A"
	cd "$WORKDIR"
	mkdir -p "$P"
	tar xaf 'data.tar.xz' -C "$WORKDIR/$P"
	rm 'data.tar.xz'
	cd -
}

src_install() {
	cp -a "$S/usr" "$D"

	udev_dorules "$S/etc/udev/rules.d/99-flashforge5.rules"
}

pkg_postinst() {
		xdg_icon_cache_update
		xdg_desktop_database_update
		xdg_mimeinfo_database_update
}

pkg_postrm() {
		xdg_icon_cache_update
		xdg_desktop_database_update
		xdg_mimeinfo_database_update
}
